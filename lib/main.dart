import 'package:flutter/material.dart';
import 'dart:io';
import 'routes/categories.dart';
import 'routes/maps/simple_map.dart';
import 'routes/history.dart';
import 'routes/scan.dart' as sc;
import 'package:permission_handler/permission_handler.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SiteLocator',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeData(title: "SiteLocator"),
      debugShowMaterialGrid: false,
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomeData extends StatefulWidget {
  HomeData({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _Home createState() => _Home();
}

class _Home extends State<HomeData> {
  _RequestPermissions() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([
      PermissionGroup.camera,
      PermissionGroup.locationWhenInUse,
      PermissionGroup.location,
      PermissionGroup.locationAlways,
      PermissionGroup.storage,
      PermissionGroup.photos
    ]);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _RequestPermissions();
  }

  void _MenuActions(String value) {
    switch (int.parse(value)) {
      case 0:
        {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => History()),
          );
          break;
        }
      case 1:
        {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MapSample()),
          );
          break;
        }
      case 2:
        {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MainFetchData(
                      title: "SiteLocator",
                    )),
          );
          break;
        }
      case 3:
        {
          exit(0);
          break;
        }
    }
  }

  Dialog errorDialog;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            widget.title + "\nEscaner de codigos QR",
            style: TextStyle(fontSize: 16.0),
          ),
          actions: <Widget>[
            PopupMenuButton(
              icon: Icon(Icons.more_vert),
              itemBuilder: (_) => <PopupMenuItem<String>>[
                new PopupMenuItem<String>(
                    child: const Text('Historial'), value: '0'),
                new PopupMenuItem<String>(
                    child: const Text('Mapa Simple'), value: '1'),
                new PopupMenuItem<String>(
                    child: const Text('Lugares de Interes'), value: '2'),
                new PopupMenuItem<String>(
                    child: const Text('Salir'), value: '3'),
              ],
              onSelected: _MenuActions,
            )
          ],
        ),
        body: Center(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Padding(padding: new EdgeInsets.fromLTRB(0, 0, 0, 50)),
            new Image.asset(
              'assets/images/logo.png',
              width: 350,
              alignment: Alignment.center,
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 25),
            ),
            RaisedButton(
              padding:
                  EdgeInsets.only(top: 15, bottom: 15, right: 60, left: 60),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0)),
              child: Text(
                "ESCANEAR",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              color: Colors.redAccent,
              textColor: Colors.white,
              onPressed: () {
                sc.getScan(context);
                //sc.getScan(context);
              },
            )
          ],
        )));
  }
}
