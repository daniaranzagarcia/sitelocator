import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'sub_categories.dart';

class MainFetchData extends StatefulWidget {
  MainFetchData({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MainFetchDataState createState() => _MainFetchDataState();
}

class _MainFetchDataState extends State<MainFetchData> {
  List list = List();
  var isLoading = false;
  var cond = true;

  _fetchData() async {
    if (cond) {
      cond = false;
      setState(() {
        isLoading = true;
      });
      final response = await http
          .get("https://ebostpay.herokuapp.com/listarCategorias/LOCATOR@eBost");
      if (response.statusCode == 200) {
        var responseJson = json.decode(response.body);
        list = responseJson["data"] as List;
        setState(() {
          isLoading = false;
        });
      } else {
        throw Exception('Failed to load data!');
      }
    }
  }

  void _openSubCategory(List sub) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SubMainFetchData(
            title: "SiteLocator",
            sub: sub,
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    _fetchData();

    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: isLoading
            ? Center(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(50),
                    ),
                    Text("Cargando..."),
                    Padding(
                      padding: EdgeInsets.all(15),
                    ),
                    CircularProgressIndicator()
                  ],
                ),
              )
            : ListView.separated(
                padding: const EdgeInsets.all(16),
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: new Text(list[index]['categoria_es']),
                    onTap: () {
                      _openSubCategory(list[index]['subcategorias']);
                    },
                    subtitle: new Text("Tipo: " + list[index]['tipo_categ']),
                    leading: Icon(Icons.arrow_right,),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(
                      thickness: 1,
                    )));
  }
}
