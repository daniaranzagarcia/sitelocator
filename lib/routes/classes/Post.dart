
class Post {
  final String descr, art;
  final List images;

  Post({
    this.descr,
    this.art,
    this.images,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return new Post(
      descr: json['data']['descripcion']["es"].toString(),
      art: json['data']['metadatos'][0]["valor"]["es"].toString(),
      images: json['data']['galeria']['imagenes'] as List,
    );
  }
}