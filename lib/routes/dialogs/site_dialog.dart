import 'package:flutter/material.dart';
import 'dart:io';

import 'package:sitelocator/routes/classes/Post.dart';
import 'package:url_launcher/url_launcher.dart';

class SiteDialogOverlay extends StatefulWidget {
  SiteDialogOverlay(
      {this.id, this.lat, this.lng, this.title, this.mail, this.web, this.post})
      : super();

  final String id, lat, lng, title, mail, web;
  Future<Post> post;

  @override
  State<StatefulWidget> createState() => SiteDialogOverlayState(
      id: id,
      lat: lat,
      lng: lng,
      mail: mail,
      title: title,
      web: web,
      post: post);
}

class SiteDialogOverlayState extends State<SiteDialogOverlay>
    with SingleTickerProviderStateMixin {
  SiteDialogOverlayState(
      {this.id, this.lat, this.lng, this.title, this.mail, this.web, this.post})
      : super();

  final String id, lat, lng, title, mail, web;
  Future<Post> post;
  AnimationController controller;
  Animation<double> scaleAnimation;

  String _img = '';
  var cond = true;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    Container _noImg = new Container(
      child: Text("Sin Imagenes", textAlign: TextAlign.center),
      decoration: new BoxDecoration(
          borderRadius: new BorderRadius.all(new Radius.circular(10.0)),
          color: Color.fromRGBO(0, 0, 0, 0.05)),
      padding: new EdgeInsets.all(16),
    );

    return new Dialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
      child: new Container(
        margin: EdgeInsets.only(top: 0, right: 5, left: 5, bottom: 15),
        child: Stack(
          children: <Widget>[
            Positioned(
              right: 0.0,
              top: 5.0,
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Align(
                  alignment: Alignment.topRight,
                  child: CircleAvatar(
                    radius: 14.0,
                    backgroundColor: Colors.white,
                    child: Icon(
                      Icons.close,
                      color: Colors.black54,
                      size: double.minPositive + 20,
                    ),
                  ),
                ),
              ),
            ),
            new FutureBuilder<Post>(
              future: post,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return (new Column(
                    children: <Widget>[
                      new Padding(padding: EdgeInsets.only(top: 30)),
                      new Text(
                        title + "\n",
                        style: TextStyle(fontSize: 14),
                        textAlign: TextAlign.center,
                      ),
                      new InkWell(
                        child: new SelectableText(
                          "" + web,
                          style:
                              TextStyle(fontSize: 12, color: Colors.blueAccent),
                          textAlign: TextAlign.center,
                        ),
                        onTap: () => launch('$web'),
                      ),
                      new SelectableText(
                        "" + mail + "\n",
                        style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,
                      ),
                      (snapshot.data.images.isNotEmpty)
                          ? (new SizedBox(
                              width: MediaQuery.of(context).size.width,
                              height: 200,
                              child: FadeInImage.assetNetwork(
                                width: MediaQuery.of(context).size.width,
                                placeholder: 'assets/images/spin_loading.gif',
                                image: (cond)
                                    ? 'http://file-s.download' +
                                        snapshot.data.images[0]['imagen']
                                    : _img,
                              ),
                            ))
                          : _noImg,
                      (snapshot.data.images.isNotEmpty)
                          ? new Flexible(
                              child: new SizedBox(
                              height: 100,
                              width: MediaQuery.of(context).size.width,
                              child: ListView.builder(
                                itemCount: snapshot.data.images.length,
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemBuilder: (BuildContext context, int index) {
                                  return new InkWell(
                                    onTap: () {
                                      setState(() {
                                        _img = 'http://file-s.download' +
                                            snapshot.data.images[index]
                                                ['imagen'];
                                        cond = false;
                                      });
                                    },
                                    child: FadeInImage.assetNetwork(
                                      placeholder:
                                          'assets/images/spin_loading.gif',
                                      placeholderScale: 0.2,
                                      image: 'http://file-s.download' +
                                          snapshot.data.images[index]['imagen'],
                                      width: MediaQuery.of(context).size.width *
                                          0.4,
                                      fit: BoxFit.fitWidth,
                                      repeat: ImageRepeat.noRepeat,
                                    ),
                                  );
                                },
                              ),
                            ))
                          : new Text(""),
                      new Expanded(
                          flex: 1,
                          child: new SingleChildScrollView(
                            child: new SelectableText(
                              "" + snapshot.data.descr,
                              style: TextStyle(fontSize: 12),
                              textAlign: TextAlign.center,
                            ),
                          )),
                    ],
                  ));
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }

                // By default, show a loading spinner.
                return CircularProgressIndicator();
              },
            )
          ],
        ),
      ),
    );
  }
}
