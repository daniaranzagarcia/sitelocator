import 'dart:async';
import 'dart:io' as io;

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sitelocator/routes/maps/map.dart';
import 'package:sitelocator/main.dart';

class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  bool isLoading = true;
  bool cond = true;
  String directory;
  List file = new List();
  List files = new List();
  List names = new List();
  List datas = new List();
  String id = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _listFiles();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => new MyApp()));
            }),
        title: Text("Historial"),
      ),
      body: isLoading
          ? Center(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(50),
                  ),
                  Text("Cargando..."),
                  Padding(
                    padding: EdgeInsets.all(15),
                  ),
                  CircularProgressIndicator()
                ],
              ),
            )
          : Container(
              child: Column(
              children: <Widget>[
                // your Content if there
                Expanded(
                  child: ListView.separated(
                      padding: EdgeInsets.only(
                          top: 16.0, bottom: 16.0, right: 5, left: 8),
                      separatorBuilder: (context, index) => Divider(
                            color: Colors.grey,
                          ),
                      itemCount: datas.length,
                      itemBuilder: (BuildContext context, int index) {
                        var dataArr = datas[index].toString().split(";");

                        return ListTile(
                          title: Text(dataArr[3]),
                          subtitle: Text(dataArr[5]),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Map(
                                        id: dataArr[0],
                                        lat: dataArr[1],
                                        lng: dataArr[2],
                                        title: dataArr[3],
                                        mail: dataArr[4],
                                        web: dataArr[5],
                                      )),
                            );
                          },
                          onLongPress: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text("Estas seguro?"),
                                  content:
                                      Text("Presione 'Borrar' para confirmar"),
                                  actions: [
                                    new FlatButton(
                                        onPressed: () => Navigator.pop(context),
                                        child: Text("Cancelar")),
                                    new FlatButton(
                                        onPressed: () {
                                          final dir = io.Directory(directory +
                                              '/hys' +
                                              dataArr[0] +
                                              '.txt');
                                          dir.deleteSync(recursive: true);
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      new MyApp()));
                                        },
                                        child: Text("Borrar"))
                                  ],
                                );
                              },
                            );
                          },
                          leading: Icon(
                            Icons.location_on,
                            color: Colors.redAccent,
                          ),
                        );
                      }),
                ),
              ],
            )),
      floatingActionButton: isLoading
          ? null
          : FloatingActionButton.extended(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text("Estas seguro?"),
                      content: Text("Presione 'Borrar' para confirmar"),
                      actions: [
                        new FlatButton(
                            onPressed: () => Navigator.pop(context),
                            child: Text("Cancelar")),
                        new FlatButton(
                            onPressed: () {
                              final dir = io.Directory(directory + '/');
                              dir.deleteSync(recursive: true);
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => new MyApp()));
                            },
                            child: Text("Borrar"))
                      ],
                    );
                  },
                );
              },
              label: Text("Limpiar Historial"),
              icon: Icon(Icons.delete),
            ),
    );
  }

  Future<void> _listFiles() async {
    if (cond) {
      cond = false;
      directory = (await getApplicationDocumentsDirectory()).path;
      file = io.Directory("$directory/")
          .listSync(); //use your folder name insted of resume.

      for (var f in file) {
        f.toString().contains("hys") ? files.add(f) : null;
      }
      await _nameFiles();
    }
  }

  Future<void> _nameFiles() async {
    for (var f in files) {
      var arr = f
          .toString()
          .split("File: '/data/user/0/app.pkg.sitelocator/app_flutter/");
      var name = arr[1].split("'");
      names.add(name[0]);
      await _dataFiles(name[0]);
      //print(name[0]);
    }

    setState(() {
      isLoading = false;
    });
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<io.File> get _localFile async {
    final path = await _localPath;
    return io.File('$path/' + id);
  }

  Future<String> readcontent() async {
    try {
      final file = await _localFile;
      // Read the file
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      print(e.toString());
      return 'Error:';
    }
  }

  Future<void> _dataFiles(String n) async {
    id = n;
    await readcontent().then((String value) {
      datas.add(value);
      print(value);
    });
  }
}
