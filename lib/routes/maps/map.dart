import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sitelocator/routes/classes/Post.dart';
import 'package:sitelocator/routes/dialogs/site_dialog.dart';
import 'package:http/http.dart' as http;

class Map extends StatefulWidget {
  Map({Key key, this.id, this.lat, this.lng, this.title, this.mail, this.web})
      : super(key: key);

  final String id, lat, lng, title, mail, web;

  @override
  State<Map> createState() =>
      MapState(id: id, lat: lat, lng: lng, mail: mail, title: title, web: web);
}

class MapState extends State<Map> {
  MapState({this.id, this.lat, this.lng, this.title, this.mail, this.web})
      : super();

  final String id, lat, lng, title, mail, web;

  Completer<GoogleMapController> _controller = Completer();

  Position _position = new Position();
  var isLoading = true;

  Set<Marker> markers = Set();

  Marker site;
  Marker start;

  void addMarker() {
    site = new Marker(
        onTap: () {
          _showMsg("Tu Posicion Inicial");
        },
        markerId: MarkerId('Tu Posicion Inicial'),
        position: LatLng(_position.latitude, _position.longitude));

    start = Marker(
        onTap: () {
          _showMsg("" + title);
        },
        markerId: MarkerId('' + title),
        position: LatLng(double.parse(lat), double.parse(lng)));

    markers.addAll([site, start]);
  }

  _getLocation() async {
    _position = await Geolocator()
        .getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    _checkGPS();
    addMarker();
    return new Scaffold(
      appBar: AppBar(
        title: Text(
          "SiteLocator",
          style: TextStyle(fontSize: 14),
          textAlign: TextAlign.center,
        ),
        centerTitle: false,
        actions: <Widget>[
          new FlatButton.icon(
            onPressed: () {
              showDialog(
                context: context,
                builder: (_) => SiteDialogOverlay(
                  web: web,
                  mail: mail,
                  title: title,
                  lng: lng,
                  lat: lat,
                  id: id,
                  post: post,
                ),
              );
            },
            icon: Icon(Icons.not_listed_location),
            label: Text(
              "Mas Detalles\ndel Sitio",
              style: TextStyle(fontSize: 14),
            ),
            textColor: Colors.white,
            disabledTextColor: Colors.white,
          )
        ],
      ),
      body: (isLoading && !_GPS)
          ? Center(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(50),
                  ),
                  Text("Cargando..."),
                  Padding(
                    padding: EdgeInsets.all(15),
                  ),
                  CircularProgressIndicator(),
                ],
              ),
            )
          : GoogleMap(
              mapType: MapType.normal,
              myLocationEnabled: true,
              compassEnabled: true,
              buildingsEnabled: true,
              indoorViewEnabled: true,
              mapToolbarEnabled: true,
              myLocationButtonEnabled: false,
              rotateGesturesEnabled: true,
              trafficEnabled: false,
              zoomGesturesEnabled: true,
              scrollGesturesEnabled: true,
              tiltGesturesEnabled: true,
              initialCameraPosition: CameraPosition(
                  target: LatLng(_position.latitude, _position.longitude),
                  zoom: 19.151926040649414),
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              markers: markers,
            ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Container(
        child: Row(
          textDirection: TextDirection.rtl,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 0, 130),
            ),
            RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              child: Row(
                children: <Widget>[
                  const Text("Localizarme"),
                  Icon(Icons.location_on)
                ],
              ),
              onPressed: _goToMyLocation,
              color: Colors.redAccent,
              textColor: Colors.white,
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
            ),
            RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              child: Row(
                children: <Widget>[
                  const Text("Ir al Sitio"),
                  Icon(Icons.location_on)
                ],
              ),
              onPressed: () {
                _goToSiteLocation();
              },
              color: Colors.blueAccent,
              textColor: Colors.white,
            ),
          ],
        ),
      ),
    );
  }

  void _showMsg(String msg) {
    Fluttertoast.showToast(
        msg: '' + msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.black54,
        textColor: Colors.white);
  }

  Future<void> _goToSiteLocation() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(new CameraPosition(
        target: LatLng(double.parse(lat), double.parse(lng)), zoom: 18)));
  }

  Future<void> _goToMyLocation() async {
    _getLocation();
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(new CameraPosition(
        target: LatLng(_position.latitude, _position.longitude), zoom: 18)));
  }

  //Service Methods
  var list = List();
  var isLoading2 = false;
  var cond2 = true;

  Future<Post> post;

  Future<Post> _fetchData() async {
    if (cond2) {
      cond2 = false;
      setState(() {
        isLoading2 = true;
      });
      final response = await http.get(
          "https://ebostpay.herokuapp.com/listarInfoPlan/LOCATOR01-1-" +
              id +
              "/LOCATOR@eBost/tarifas/no");
      if (response.statusCode == 200) {
        // If server returns an OK response, parse the JSON.
        return Post.fromJson(json.decode(response.body));
      } else {
        // If that response was not OK, throw an error.
        throw Exception('Failed to load post');
      }
    }
  }

  var _GPS = false;
  _checkGPS() async {
    GeolocationStatus geolocationStatus =
        await Geolocator().checkGeolocationPermissionStatus();
    if (geolocationStatus == GeolocationPermission.locationWhenInUse ||
        geolocationStatus == GeolocationPermission.locationAlways ||
        geolocationStatus == GeolocationPermission.location) {
      setState(() {
        this._GPS = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _checkGPS();
    _getLocation();
    post = _fetchData();

  }
}
