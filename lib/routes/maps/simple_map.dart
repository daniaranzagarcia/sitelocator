import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  Completer<GoogleMapController> _controller = Completer();

  Position _position;
  var isLoading = true;
  _getLocation() async {
    _position = await Geolocator()
        .getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    _checkGPS();
    return new Scaffold(
      appBar: AppBar(
        title: Text(
          "SiteLocator\nMapa",
          style: TextStyle(fontSize: 16),
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
      ),
      body: !_GPS
          ? (isLoading
              ? Center(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(50),
                      ),
                      Text("Cargando..."),
                      Padding(
                        padding: EdgeInsets.all(15),
                      ),
                      CircularProgressIndicator()
                    ],
                  ),
                )
              : GoogleMap(
                  mapType: MapType.normal,
                  myLocationEnabled: true,
                  compassEnabled: true,
                  buildingsEnabled: true,
                  indoorViewEnabled: true,
                  mapToolbarEnabled: true,
                  myLocationButtonEnabled: false,
                  rotateGesturesEnabled: true,
                  trafficEnabled: false,
                  zoomGesturesEnabled: true,
                  scrollGesturesEnabled: true,
                  tiltGesturesEnabled: true,
                  initialCameraPosition: CameraPosition(
                      target: LatLng(_position.latitude, _position.longitude),
                      zoom: 19.151926040649414),
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                ))
          : Text("Enable Location And Retry!"),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _goToMyLocation,
        label: Text('Localizarme!'),
        icon: Icon(Icons.location_on),
      ),
    );
  }

  Future<void> _goToMyLocation() async {
    _getLocation();
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(new CameraPosition(
        target: LatLng(_position.latitude, _position.longitude), zoom: 20)));
  }

  var _GPS = false;
  _checkGPS() async {
    GeolocationStatus geolocationStatus =
        await Geolocator().checkGeolocationPermissionStatus();
    if (geolocationStatus == GeolocationPermission.locationWhenInUse ||
        geolocationStatus == GeolocationPermission.locationAlways ||
        geolocationStatus == GeolocationPermission.location) {
      setState(() {
        this._GPS = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _checkGPS();
    _getLocation();
  }
}
