import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sitelocator/routes/maps/map.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qrscan/qrscan.dart' as scanner;

void getScan(BuildContext context) {
  _getResult(context);
}

String id = '';

_getResult(BuildContext context) async {
  try {
    String _result = await scanner.scan();
    var scan = _result.split(";");
    writeContent(_result);
    id = scan[0];
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => Map(
                id: scan[0],
                lat: scan[1],
                lng: scan[2],
                title: scan[3],
                mail: scan[4],
                web: scan[5],
              )),
    );
  } catch (Exception) {}
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();
  // For your reference print the AppDoc directory
  print(directory.path);
  return directory.path;
}

Future<File> get _localFile async {
  final path = await _localPath;
  return File('$path/hys'+id+'.txt');
}

Future<File> writeContent(String data) async {
  final file = await _localFile;
  // Write the file
  return file.writeAsString(data);
}


