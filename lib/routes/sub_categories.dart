import 'package:flutter/material.dart';
//import 'package:fluttertoast/fluttertoast.dart';

class SubMainFetchData extends StatefulWidget {
  SubMainFetchData({Key key, this.title, this.sub}) : super(key: key);

  final String title;
  final List sub;

  @override
  _SubMainFetchDataState createState() => _SubMainFetchDataState(sub: sub);
}

class _SubMainFetchDataState extends State<SubMainFetchData> {
  _SubMainFetchDataState({this.sub}) : super();

  final List sub;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ListView.separated(
            padding: const EdgeInsets.all(16),
            itemCount: sub.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: new Text(sub[index]['subcateg_es']),
                leading: Icon(
                  Icons.arrow_right,
                ),
                onTap: () {
                  /*


                  Fluttertoast.showToast(
                      msg: ''+sub[index]['subcateg_es'],
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIos: 1,
                      backgroundColor: Colors.black54,
                      textColor: Colors.white
                  );
                   */
                },
              );
            },
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(
                  thickness: 1,
                )));
  }
}
